# Basic JavaScript

### Task 1
Menghitung luas segitiga

Rumus :
<img src="https://render.githubusercontent.com/render/math?math=\Large \frac{1}{2}.a.t">
<hr>

### Task 2
Menghitung luas lingkaran

Rumus :
<img src="https://render.githubusercontent.com/render/math?math=\Large \pi.r^2">
<hr>

### Task 3
Membuat dan Menampilkan Variabel Tipe Data Object

Variabel bertipe object adalah variabel yang dapat menyimpan koleksi atau sekumpulan data berdasarkan _key-value_ untuk setiap item. Pada kasus ini akan menampilkan variabel detailMovie yang memiliki 5 _key-value_ yaitu: _title_, _synopsis_, _release_, _genre_, dan _starring_.
<hr>

### Task 4
Menampilkan Array dengan _forEach_

_forEach_ adalah perulangan untuk setiap item pada _array_ berdasarkan urutan. Pada program ini menggunakan _forEach_ untuk menampilkan masing-masing item dari 2 _array_ secara berpasangan yaitu _array_ _actor_ dan _role_.
// Fungsi untuk menghitung luas segitiga
function luasSegitiga() {
    let alas = document.getElementById("alas").value;
    let tinggi = document.getElementById("tinggi").value;
    var hasilLuasSegitiga;

    if (alas == "") {
        alert("tentukan nilai alas segitiga");
    } else if (tinggi == "") {
        alert("tentukan nilai tinggi segitiga");
    } else {
        hasilLuasSegitiga = 0.5 * Number(alas) * Number(tinggi);
    }
    document.getElementById("result-segitiga").innerHTML = hasilLuasSegitiga;
}


// Fungsi untuk menghitung luas lingkaran
function luasLingkaran() {
    let jari = document.getElementById("jari").value;
    var hasilLuasLingkaran;

    if (jari == "") {
        alert("tentukan nilai jari-jari lingkaran");
    } else {
        hasilLuasLingkaran = Math.PI * Number(jari)**2;
    }
    document.getElementById("result-lingkaran").innerHTML = hasilLuasLingkaran;
}


const detailMovie = {
    title: "Avengers Infinity War",
    synopsis: "With the powerful Thanos on the verge of raining destruction upon the universe, the Avengers and their Superhero allies risk everything in the ultimate showdown of all time.",
    releaseYear: 2018,
    genre: ["Superhero", "Action"],
    starring: {
        actor: ["Chris Evans", "Tom Holland", "Benedict Cumberbatch", "Robert Downey Jr", "Scarlett Johansson"],
        role: ["Captain America", "Spiderman", "Doctor Strange", "Iron Man", "Black Widow"]
    }
};

// Membuat data object dan menampilkannya
function tampilkanObject() {
    document.getElementById("title").innerHTML = detailMovie.title;
    document.getElementById("synopsis").innerHTML = detailMovie.synopsis;
    document.getElementById("release").innerHTML = detailMovie.releaseYear;
    document.getElementById("genre").innerHTML = detailMovie.genre;
    document.getElementById("actor").innerHTML = detailMovie.starring.actor;
    document.getElementById("role").innerHTML = detailMovie.starring.role;
}

function tampilkanArray() {
    // document.getElementById("result-array").innerHTML = `Pada film ${detailMovie.title}, ${detailMovie.starring.actor[0]} memerankan ${detailMovie.starring.role[0]}.`;
    var actor = detailMovie.starring.actor;
    var role = detailMovie.starring.role;

    actor.forEach((actorName, idx) => {
        const actorRole = role[idx];
        document.getElementById("result-array").innerHTML += `${actorName} berperan sebagai ${actorRole}.<br>`;
    });
}